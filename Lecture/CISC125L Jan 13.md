# Flow of control

## Sequential
Code runs line by line. ( sequential operation)

## Selection
We use statements like `if`, `else`, `elseif`, `switch` so select which pieces of code run.

## Repetition
We can use ***pre-condition*** (`for`, `while`) or ***post condition*** (`do-while`) loops to repeat code.

# Identifiers

`int`, `char`, `double` are ***type identifiers***.

For example, `n` identifies a variable.

`10` or `6.34` identify constant values of type ***integer*** or ***double*** respectively.

# Some basic C++ syntax

## Pointer variables

***Pointer variables*** hold a memory address:

```c++
int grades[10];
```

`grades` in the above code is a pointer variable, that points to a sequence of 10 cells holding a single integer each.

> The address of this variable is the address of the first cell.

> We can use the ***indexing operator*** ( `[n]` ) to select a cell and the ***assignment operator*** ( `=` ).
> `int grades[4] = 96;`


## Operators

### Associativity and precedence

One of the issues with operators is ***precedence*** ( the order in which they run). In order to know what an expression evaluates to, we must know the *precedence of the operator*.

We also have to worry about the ***associativity***. Most operators associate *left to right*.

> For example we assume that `5<4<3` is true; however, because of the left to right associativity, this would evaluate to true. This must then be converted to `5<4 && 4<3` in order to evaluate correctly.

We can use ***parentheses*** to force precedence.

### Operator types

***Unary operators*** take one operand. `!A`
***Binary operators*** take two operands. `A+B`
***Trinary operators*** take three operands. `exp ? A: B;`

### Structs

In order to package different types into a single object, C++ gives us the `struct` keyword. A struct can have ***members*** of different types.

```c++
struct personType
{
    int age;
    string name;
    char married;
};
```

> Note that `personType` is a type identifier just like `char`, `int`, or `bool`. Now we can declare variables of type `personType` ( the compiler will allocate the memory for the members ).

To access the members of a `struct` we use the ***dot operator*** ( `.` ).

```c++
int main()
{
    personType Joe; // Declare variable Joe as a personType

    Joe.age = 25; // Sets the value of age to 25
}
```

Suppose we want a function that tells me the age of anything of a `personType`... I missed this part but we could probably create a function that takes a `personType` by reference: 

```c++
#include <iostream>

using namespace std;

struct personType
{
    int age;
    string Name;
    char married;
};

int getPersonAge(personType* person) {
    return person.age;
}

int main(){
    personType Joe;
    Joe.age = 25;

    cout << Joe.age << endl;

    return 1;
}
```