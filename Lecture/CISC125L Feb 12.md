# **CISC125 Lecture Feb. 12, 2020**

# Error in my second assignment ( `anachronism used` )

https://wiki.sei.cmu.edu/confluence/display/cplusplus/DCL52-CPP.+Never+qualify+a+reference+type+with+const+or+volatile

I prototype my stream insertion operator as:

```c++
friend ostream& operator << (ostream&, dayType& const);
```

The way my code is written, I have the address operator ( `&` )before the `const` qualifier. **This is wrong.**  The address operator should go right before the variable or parameter name ( meaning it should be last in the parameter in the prototype ).

```c++
friend ostream& operator << (ostream&, dayType const &);
```

Unfortunately, my compiler merely gives a quiet warning:

```c++
warning C4227: anachronism used : qualifiers on reference are ignored
```

So, it sees that I put the address operator in the wrong place and, to correct it, **it just ignores the `const` qualifier during compilation.**


# More arrays

When we use the index operator ( `[n]` ) it takes the address of the variable and increments it by n times the size of the variable. For example, let's say we make an integer array whose first address is at `10000` (that is the 10000th byte in memory.)...

```c++
int grades[10];
```

An integer is 4 bytes large so using getting the 4th element of the array ( `grades[3]` ) gives the address at 10000 + 3 * 4 or 10012.

## Multidimensional arrays

We can define multidimensional arrays by chaining index operators:

```c++
int sheet[size_m][size_n];
```

When we use the index operators we call the element in the address base_address + (n + m*size_n)*int_size. Therefore, if we again had an `int` matrix at `10000` and we ran...

```c++
int matrix[4][10];
matrix[1][3]; // 10000 + (3 + 1*10)*4 == 10052
```

# More pointers

We can accomplish the same thing using an array of pointers. ( Who would do this? )

First we should know that we can make a pointer to a pointer:
```c++
int ** WhyThough;
```

```c++
int * matrix[4]; // Array of 4 pointers
```

We can then create an array for each pointer element:

```c++
for(int i = 1; i < 4; i++)
    matrix[i] = new int [10];
```

# Shallow and deep copies

If we don't write our own assignment operator, the system will automatically create a **shallow copy** which will copy the values of the members into the other class.

This can become a problem if the object contains a pointer since the address from the first object will end up in the second object. If we modify the value in the pointer for the second object we'll modify the first object.

To avoid this we need a deep copy.

# Early vs Late binding

When a function decides what function to run or what decision to make during compile time, this is called **early binding**. When this is decided during run time it is called **late binding**.

In order to force the compiler to make the decision later we need to specify the function as `virtual`.

```c++
class petType {
public:
    virtual void print();
}
```

# Virtual functions

Virtual functions can be declared using the syntax above. When we put these functions in a class, we turn the class into an abstract class so we can't create an instance of the object.