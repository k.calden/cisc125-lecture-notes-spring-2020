# Assignment ( Due Feb. 3, 2020) See snippets at the bottom

Chapter 10 programming exercises 7 & 8 on page 737
> In the sixth edition, this corresponds to 4 & 5 in chapter 10 programming exercises

# Code for the assignment 

#### Class definition
```c++
using namespace std;

class dayType
{
    public:

        void setDay(const string &) const; // Set the day
        void printDay(ostream &) const; // Print stream
        String getDay() const; // Return the name of the day
        String nextDay() const; // Return the name of the next day
        String previousDay() const; // Return the name of the previous day
        String add(int) const; // Return the name of the day after 13 days

        // CONSTRUCTORS
        dayType(); // Set the day to 0, "Saturday"
        dayType(const dayType &);
        dayType(const String &);
    
    private:
        String dayName;
        int dayNumber; // Saturday is 0
};

dayType::dayType(){ dayName = "Saturday"; dayNumber = 0; }

void dayType::setDay(const String & name) {
    if(day == "Saturday") { dayName = name; dayNumber = 0}
    else if(day == "Sunday") { dayName = name; dayNumber = 1}
    else if(day == "Monday") { dayName = name; dayNumber = 2}
    else if(day == "Tuesday") { dayName = name; dayNumber = 3}
    else if(day == "Wednesday") { dayName = name; dayNumber = 4}
    else if(day == "Thursday") { dayName = name; dayNumber = 5}
    else if(day == "Friday") { dayName = name; dayNumber = 6}
}

void dayType::printDay(ostream & out) {
    out << "The name of the day is " << dayName << std::endl;
}

String dayType::getDay() {
    return dayName;
}

String dayType::nextDay(){
    switch(dayNumber){
        case 0: return "Sunday"; break;
        case 1: return "Monday"; break;
        case 2: return "Tuesday"; break;
        case 3: return "Wednesday"; break;
        case 4: return "Thursday"; break;
        case 5: return "Friday"; break;
        case 6: return "Saturday"; break;
    }
}
```
> The `printDay` function is passed an ostream because we want to be able to edit a file or write to `cout`.

#### Client code

```c++
#include <fstream>

int main(){
    ofstream outFile("filename");

    dayType day1;

    day1.setDay("Tuesday");
    day1.printDay();
}
```

# Exercise ( Holding a rational number )

Let's say we wanted to hold a number as a rational number ( integer divided by an integer ).

We want the number to be able give its numerator and denominator. Also, the number should always be in reduced form.

## Forcing the number to be in reduced form

In the definition of the class, we can write a `private member function` that reduces the number and run this function every time we run an operation.