# Assignment due Monday, Jan. 10, 2020

Overload the extraction and insertion operators for the `dayType` class.

# CISC125 Lecture (Jan. 3, 2020)

## Note for all future assignments

- Put your name as a comment in the first line of the program
- Put your name on the output
- Note compile errors
- Close the file
- Throw error if the file doesn't open

# Overloading the extraction and insertion operator

Recall the `dayType` class. We wouldn't be able to print it to a stream directly since we don't have a stream insertion operator for that type, that is, we can run this line:

```c++
dayType d1;
cout << d1;
```

In order to pull a `dayType` from a stream we would need a stream extraction operator.

**Function signatures for a stream extraction and stream insertion operator ( for the rational class )**

```c++
istream& operator >> (istream &is, rational &r);
ostream& operator << (ostream &os, const rational &r);
```

**Implementation for these two operators**
```c++
ostream& operator << (ostream &os, const rational &r)
{
    os << r.numerator() << '/' << r.denominator();
    return os;
}

istream& operator >> (istream &is, rational &r) {
    char divisionSymbol;
    int numerator = 0, denominator = 0;

    is >> numerator >> divisionSymbol >> denominator;
    assert(divisionSymbol == '/'); // Make sure that we have a proper division symbol
    assert(denominator != 0); // Make sure that we don't divide by zero
    rational number(numerator, denominator);
    r = number;
    return is;
}
```

# Mixed mode arithmetic operators

Let's say we have a `rational` type object and we want to add an integer to it. We can write arithmetic operator overloads to do mixed mode arithmetic.

```c++
rational operator (const rational &lhs, int rhs)
{
    rational newOperand(rhs, 1); // Create a rational from the integer
    return lhs + newOperand; // Use our current addition operator to do sum the two rationals
}
```

# Inheritance

Let's say we have a class called `Hospital` we want to make another specialized class called `ChildrensHospital` that inherits from `Hospital`. `Hospital` is the *base class* and `ChildrensHospital` is a *derived class*.

> It's important to note that a `ChildrensHospital` is a `Hospital` but a `Hospital` is not a `ChildrensHospital`.

## `SavingsAccount`

Recall our `Account` type. We can make a `SavingsAccount` type that inherits from the `Account` type using the colon `:`.

```c++
class SavingsAccount : public Account
{
    public:

    SavingsAccount();
    SavingsAccount(const apstring &password, double balance);
    SavingsAccount(const SavingsAccount &a);

    double getInterest(const apstring &password) const;

    void computeInterest(double rate);

    private:
    double myInterest;
}
```

> The `public` inheritance says what the public members of the base class are transferred to the derived class as. None of the private members of the base class are accessible in the derived class.

We can invoke the constructor for the base class in the constructor for the derived class by again using the colon.

```c++
SavingsAccount::SavingsAccount() : Account() {
    // ....
}
```

In the copy constructor for a `SavingsAccount`, we should pass the base class copy constructor the `Account`:

```c++
SavingsAccount::SavingsAccount(const SavingsAccount &a) : Account(a)
{
    myInterest = a.myInterest;
}
```

### `protected` members

If the `private` members were `protected` instead, we would be able to manipulate the directly in the derived class.

