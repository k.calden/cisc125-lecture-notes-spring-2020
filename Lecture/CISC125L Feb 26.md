# ( Feb. 26, 2020 )
# Assingment (due Mar. 2)

Chapter 14 programming exercise #4

# Templates
## Template functions

The concept of a template function is that you can write one function that can do the same thing on different types. To make one we use the `template` keyword and in angle brackets we name a generic type: 

```c++
template<class Type>
Type larger(Type x, Type y);
```

This must be done before each function and function prototype, therefore, we must do this before the implementation as well:

```c++
template<class Type>
Type larger(Type x, Type y) {
    if (x >= y) 
        return x;
    else
        return y;
}
```

> **The class must have the operators defined on it.**

To use the function, you must now tell it what type the function is:

```c++
int main() {
    larger<int>(1, 3);
    return 0;
}
```

## Template classes

We can do the same thing for classes using the same syntax, for example, we would use this for some type of list class:

```c++
template<class Type>
class listType{
    //...
};
```

# Exception handling

Sometimes we want to run some code that may not compile or produce a runtime error but we don't want the program to stop when this happens. C++ allows us to catch and throw errors and handle them the way we want to using the keywords `try`, `catch`, and `throw`. 

```c++
try{
    throw (expression that returns variable of certain type)
}
catch(type)
{
    // Code to handle the error of the certain type
}
```

> Note that we are not catching a specific message, but a certain type. We can define custom exception classes to make the code readable just by making an empty class with our name.


