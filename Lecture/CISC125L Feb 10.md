# CISC 125 Lecture Notes

# Assignment 3

Derived `payDay` from `dayType` and give it 3 new constructors:

```c++
class payDay : public dayType
{
    public:
        void setDay(string);
        void setPay(double);
        double getPay();
    private:
        double myPay;
};
```
For this class we will have to used the colon notation ( `:` ) to initialize the dayName and dayNumber that are private in the base class for example:

```c++
class payDay : public dayType
{
    public:
        payDay();
        payDay(string & dayname, double myPay);
        // ...
    private:
};

payDay::payDay() : dayType()
{
    myPay = 8.25;
}

payDay::payDay(string & dayName, double pay) : dayType(dayName) 
{
    myPay = pay
}
```
## Derived classes

Recall that we make a derived class using the following syntax:

```c++
class NewClassName : public BaseClass
{
    public:
    // ...
    private:
    // ...
}
```

# Pointers ( Chapter 12 )

Say we have a variable `n`. This variables is located at some memory address.

C++ allows us to create variables specifically for holding the address of some variable. These are called **pointers**.

```c++
int n = 10;

int *ptr_to_n;

ptr_to_n = &n;
```

> The pointer must point to the memory address of a variable of that type. We pull the memory address from that variable using the ampersand ( `&` ).

Once we have a pointer to a variable we can use the **dereference operator** ( `*` ) to modify the variable.

```c++
*ptr_to_n = 12;
// Now n == 12
```

## Why do we need pointers?

When we write a program we are allocating memory when the program is first compiled. It create memory for all of our variables.

Pointers allow us to **dynamically allocate memory** ( happens during runtime ). We can allocate memory through the `new` operator and the only way we can use this is by making a pointer to the memory address.

```c++
int * k;

k = new int;
*k = 42;
```

We can delete dynamically allocated values using the `delete` operator:

```c++
delete k;
```

If we had a pointer to an array of integers we can use the indexing operator to assign values to the array. 

```c++
int m[10];
int *k;

k[1] = 10;

// So now m[1] == 10
// Also note that this is true

k[0] == *k;
```