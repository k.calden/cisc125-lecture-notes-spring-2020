# CISC125 Lecture Jan. 29, 2020

# For the assignment 

The professor needs a paper copy of the program and the file created by the program.

## `dayType` class

```c++
class dayType
{
public:
    dayType();
    dayType(const string &);
    dayType(const dayType &);
    void setDay(const string &);
    void printDay(ostream &) const;
    string returnDay() const;
    string nextDay() const;
    string add(int) const;
    string previousDay() const;

private:
    string dayName;
    int dayNumber;
}
```

## Alternative for the day switch statement

Instead of going through a switch statement we can create an array of day strings that correspond to the days.

```c++
string dayNames[7] = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
```

# Implementing the greatest common divisor 

We can use a Euclid's algorithm to find the greatest common divisor of two numbers.

## Euclid's algorithm
Let $A$ and $B$ be two integers.
### Base case
- $GCD(A, 0) = A$
- $GCD(0, B) = B$

### Recursive case
- Let $R = A\%B$
- Find $GCD(B, R)$

> This can be optimized by forcing A to be the higher number.

# Assignment operator overload example

Recall the `rational` class, our assignment operator overload is the following:

```c++
/** Assignment operator overload
 * RETURNS: Newly assigned rational type object by constant reference
 * TAKES: rational type object by constant reference
 */
const rational & rational::operator = (const rational & rhs)
{  
    /* Compare the two objects by looking at their memory addresses. */
    if(this != &rhs) {
        myNumerator = rhs.myNumerator;
        myDenominator = rhs.myDenominator; 
    }
    /* Dereference this and return the object */
    return *this;
}
```
> **REMEMBER**, `this` refers to the memory address of the object so the pointer must be dereferenced using the `*` operator.

# Function Signature

The signature of a function is how the compiler identifies the function. This includes:

- Name of the function
- Number of parameters
- Parameter types

This allows us to do function overloading.

# The insertion operator

We cannot create an insertion operator ( `<<` ) as a member operator of a class that we define. The left hand side of this operator must be an `ostream` and the operator must return the `ostream`.

```c++
ostream& operator << (ostream &)
```