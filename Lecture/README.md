# Lecture topics

## Mar 2
- Built-in exceptions
- 

## Feb 26
- Template functions and classes
- Exception handling

## Feb 19
- Operations on dangling pointers
- ** Assigning an address to an integer

## Feb 17
- Why do we use virtual functions and abstract classes?
- Object pointers and the member access operator ( `->` )

## Feb 12
- Arrays, multidimensional arrays, and pointer arrays
- Shallow and deep copies
- Early and late binding
- Virtual functions

## Feb 10
- Derived class review
- Pointers and why we need them?

## Feb 5
- File.open(string) and File.fail()
- Default assignment operator
-  `friend` functions
-  Member access (public, private, protected)
- Intro to `struct`s and `class`es

## Feb 3
- Stream extraction and insertion operator overload
- Mixed mode arithmetic operators
- Inheritance

## Jan 29
- Assignment operator overload
- What is a function signature?
- Stream insertion operator

## Jan 27
- Class definition
- Member function implementation

## Jan 22
- Operator overloading
- Assignment chaining
- Comparing objects
- Return by constant reference (return `const`)
- Constructors
- Function prototypes
- Passing by reference
- Formal vs actual parameters
- Non-client vs Client code

## Jan 15
- Review of structs
- Introduction to pointer variables
- `class` vs `struct`
- Class constructors

## Jan 13
- Review of control structures
- Derived classes