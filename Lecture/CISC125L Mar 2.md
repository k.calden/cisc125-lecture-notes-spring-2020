# ( Mar. 2, 2020 )

**Chapter 14 Programming Exercise 4 is due on the Monday that we meet again (Mar. 9, 2020)**

# Built in exceptions

*Next exam will use this kind of exception class*

C++ has a few built in exception classes that are thrown by some of the standard functions.

`bad_alloc` can be thrown by the `new` function when a bad allocation happens ( ie. running out of memory )

`divisionByZero` will be thrown by a division operation when trying to divide by zero.

## Example of a built-in exception class
```c++
class divisionByZero { 
public:
    divisionByZero(){
        message = "Division by zero";
    }
    divisionByZero(string str) {
        message = string;
    }
    string what(){
        return message;
    }
private:
    string message;
};
```

`.what()` returns the error message from the exception class

```c++
try{
    //...
}catch(bad_alloc err) {
    cout << "Error: " << err.what() << endl;
}
```

> Error: Bad Allocation

# Specifying a throw in the function signature

We can tell the compiler that the function throws a certain set of errors.

```c++
void doDivision() throw (divisionByZero)
{
    //....
}
```