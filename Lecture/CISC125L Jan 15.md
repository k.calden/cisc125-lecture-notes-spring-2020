# Assignment

# CISC125 Lecture Notes (Jan. 15, 2020)

## Review

### Structs
Recall the following code:

```c++
struct personType
{
    int age; 
    bool married;
    string name;
};
```

> `personType` is a type identifier. Anything of that type will have these members.

We can use the class as a type when we write our `main()` function:

```c++
int main()
{
    personType Joe, Sue;
}
```

### Pointer variables

Suppose we define a pointer variable:

```c++
personType people[10]
```
The variable `people` holds an address that points to a sequence of 10 cells containing a single `personType` structure each.

In order to point to a single structure we have to use the ***indexing operator*** ( `[n]` ). This gives use the struct at the address with the offset `n`.

```mermaid
graph LR;
Address --> A[Address + 1]
A --> B[Address + 2]
B --> C[Address + n]
```

## Classes

### `class` vs `struct`

When we made a `struct`, by default, all of the members were public. We can access the members using the member access operator ( `.` ).

If we use the keyword `class` all of the members are private by default.

```c++
class personType
{
    int age;
    bool married;
    string name;
};
```

> We can't access any of these members using a `.` operator like we could with a `struct`.

In order to access these variables like a `struct` we use the `public` identifier.

```c++
class personType
{
    public:
        int age;
        bool married;
        string name;
};
```

> The `struct` keyword is a left over from ***C***. By convention, we should make any type that only holds data a `struct`.

### Member functions of classes

Classes can be used to hold data and **functions**. When we define a class, we can include function prototypes.

```c++
class personType
{
    private:
        int age;
        bool married;
        string name;
    public:
        int getAge() const; // Get the age (accessor)
        void setAge(int); // Set the age (mutator)
};
```

> We have to implement these member functions somewhere. 

> Note that we use the `const` keyword after the `.getAge()` member function prototype. We do this a safeguard so that we can change the object with this function.

We call the code that implements the member functions ***non-client code***. Non-client code can access the private members.

We can invoke member functions by using the dot operator with the function:

```c++
int main()
{
    personType Joe;

    Joe.setAge(21);
}

int personType::getAge() const
{
    return age;
}

void personType::setAge(int newAge)
{
    age = newAge;
}
```
> When invoking a member function we call `Joe` the ***receiver***.

> Note that inside the function we can just use the private member as-is.

## Example 8.2 

```c++
account judy("honeybee", 50.00);
```

In the code above code, we see a declaration of a variable called `judy` it is initializing the object using a two-parameter constructor ( in paranthese ).

### Constructors

In the class we have to have a function prototype for a constructor:

```c++
class account
{
    public:
        // Constructors
        account();
        account(const apstring &passwrd. double balance)

    private:
        apstring myPassword;
        double myBalance;
}
```

> Constructors always have the same identifiers as the class type. They also have no return type.

We can run the default constructor by omitting the parentheses:

```c++
account defaultAccount;
```