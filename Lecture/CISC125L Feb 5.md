# **CISC125 Lecture Feb. 5, 2020**

# Assignment (due Monday Feb. 10, 2020)

# Notes on previous assignment

## Checking if the file will open

```c++
ofstream outfile;
outfile.open("printdata.txt");

if(outfile.fail()) {
    cout << "File open failure";
    return 0;
}
```

## Default assignment operators

```c++
string theDay = "Sunday";
dayType day = theDay;
```

By default the assignment operator runs the appropriate constructor. Since we have a string constructor the code above will construct `day` with `theDay`.


# Member and friend operator overloads

When we make the operator overloads member functions the receiver object must be the class type. This is why we couldn't make something like the stream insertion operator ( `<<` ) a member function of the class.

That is, unless we make it a **friend**.

```c++
class dayType
{
    public:
    //...
    friend ostream& operator << (ostream&, const dayType&);
    //...
    private:
    string dayName;
    //...
}
```

Typically, we would implement this in client code then we would have to use accessor functions to access the private members. 

Using the `friend` keyword we give this function access to the private members without having to use accessors. This reduces the amount of code we have to write. Now, we can implement the function and use the private member variables like we do the public ones:

```c++
ostream & operator << (ostream& out, const dayType& day){
    out << day.dayName;
    return out;
}
```

# Member access

access | client code | member functions of base class | member functions of derived class
--- | --- | --- | --- 
public | X | X | X
protected |  | X | X
private | | X | 
