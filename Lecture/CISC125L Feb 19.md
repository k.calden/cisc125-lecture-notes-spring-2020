# ( Feb. 19, 2020 )
## For the test

Bring the handout that has the account class, rational class, and savings account class.

We can also bring a sheet of paper front and back.

The test may ask us to overload a few operators, but it won't be anything we've looked at.

# Homework review

## Operations on dangling pointers

We may run into a problem if we try to run operations on blank pointers. For example, we may get a compiler error if we try to run the following code:

```c++
int *p1;

int num1;

num1 += *p1; // p1 points to nothing so we can't perform the operation
```

## Assigning addresses to integers

The address of operator ( `&` ) returns a pointer. You can't assign a pointer to an integer so this would not compile (atleast with what we currently know):

```c++
int num1 = 30;
int num2 = &num1;
```

### How we might do this

If we try to explicitly cast the pointer to an integer (using `(int)&num1`) the compiler throws an error because of precision loss.

Integers are 4-bytes (32 bit). On a 64-bit system an address could be a number larger than an integer can hold (can't put a 64-bit number into a 32-bit space). In order to do this, we need a new type that allows us to store a 64-bit number.

We can use the `long long` type to store an address for whatever reason.

```c++
int num1 = 30;
long long addr_num1 = (long long)&num1;

std::cout << addr_num1; // Print the address of num1 as a decimal
```

# Test review

## Difference between `public`, `private`, and `protected`

`public` members can be accessed anywhere.

`private` members can be accessed only from within the class itself. This means that derivations of the class can't access the private members.

`protected` members are like the private members except they can be accessed by derivations of the class.

## Specifying inheritance with `public`, `private`, and `protected`

When deriving a class we use `public`, `private`, and `protected` to specify the inheritance.

By doing this, we are specifying what the public members of the base class will become in the derived class. Suppose we have a class:

```c++
class SomeBaseClass
{
public:
    int member1;
    double member2;
};
```

#### If we specify `public` inheritance nothing will change:

```c++
class DerivedClass1 : public SomeBaseClass{

};
```
> `member1` and `member2` will still be public.

#### If we specify `private` inheritance:

```c++
class DerivedClass2 : private SomeBaseClass{

};
```
> `member1` and `member2` will now be private

#### If we specify `protected` inheritance:

```c++
class DerivedClass3 : protected SomeBaseClass{

};
```
> `member1` and `member2` will now be protected. This means that, if we make a derived class from `DerivedClass3`, we can still access `member1` and `member2` directly within the new class.