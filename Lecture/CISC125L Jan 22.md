# CISC125 Lecture - Jan. 22, 2020


# Operator overloading

## Re-defining an operator
```c++
int main(){
    account jim("racecar", 2500.0);
    account sue(jim);
    account target;

    target = sue;

    return 0;
}
```

In the above code, we see the line `target = sue`. By default, this type of operation doesn't exist on objects of type account.

In order for this operation to take place, *we must tell the compiler how to do this operation*. We do this the same way we would write a member function into that class

```c++
const account& account::operator = (const account &a)
{
    if(this != &a)
    {
        myPassword = a.myPassword;
        myBalance = a.myBalance;
    }
    return *this;
}
```

`this` refers to `target` while `a` refers to `judy` in the function.

## Comparing objects

The line ...

```c++
if(this != &a)
```
... compares the addresses of `this` and `a`, **this is how we check to see if objects are the same**.

## Assignment chaining

In the return statement...

```c++
return *this;
```

... we are using the asterisk ( `*` ) to dereference the pointer ( returns the actual thing at the address ).

The reason why we return the object is because we want to be able to write something like...

```c++
targetA = targetB = judy;
```
... this is called **assignment chaining**.

Forcing the operator to return by constant reference won't allow us to direcly change the object returned.

## Returning assignment by constant reference

Although they are logically equivalent pieces of code, we can do this...

```c++
target = jim;
target = judy;
```

... we can't do this ...

```c++
(target = jim) = judy;
```

> I did this with integer variables `a = 1, b = 10` and found that my IDE allowed this. If I run `(a = b) = 20`, `a` is set to `20` but `b` is unchanged.

# Review - Jan. 15, 2020

## Constructors
Previously we were looking at the `account` class:


```c++
class account
{
    public:
        // Constructors
        account();
        account(const apstring &passwrd. double balance)
        account(const account &a) // Copy constructor

    private:
        apstring myPassword;
        double myBalance;
}
```
> We specified that the copy constructor gets passed an object by *constant reference*. This means that the objects members can't be changed.

Then we called the second constructor in the client code by supplying parameters in parentheses.

```c++
int main()
{
    account jim("racecar", 2000.0);
    account sue(jim);
}
```

The first line initializes `jim` based on the implementation of the constructor.

## When a constructor is called:

1. The object is brought into existence 
2. The constructor is then run on the existing object

## Copy constructors

The second line calls the ***copy constructor*** which copies the values of `jim`'s members to `sue`.
> C++ makes a default copy constructor if one isn't defined. ***The copy constructor must be used if we make a function that gets passed that object by value.***

## Why do we specify a prototype function ( constant reference? )

In a practical situation where we have teams of programmers, the person designing the classes isn't neccessarily the same person implementing the functions. This prevents collaborators from breaking code.

## Why pass by reference?

If the function prototype looked like this:

```c++
account(account q);
```

That is, if we passed the object **by value** to the function, the function would make a copy of the object while the function is running. This can make the application memory inefficient.

## Formal and Actual parameters

***Formal parameter*** - These are the parameters we define in the function prototype or implementation.

***Actual parameter*** - These are the values/objects passed to the function in client code.

## Non-client code vs client code

**Non-client code** is the code that can access private data. Think, implementation of the member functions.

Functions on objects in client code usually don't require scope resolution.

```c++
void makeWithdrawal(account &thisAccount)
{
    double amount, result;
    apstring password;

    // Code for the withdrawal prompt 

    result = thisAccount.withdraw(password, amount);
}
```

