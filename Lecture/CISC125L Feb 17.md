# CISC125 Lecture (Feb. 17, 2020)

**We are allowed to bring our account class handout to the test (Feb. 24, 2020).**


# Assignment (Due Feb. 19, 2020)
## Chapter 12 Exercises

- P893 #2
- P897 #16 
- P898 #19 

# Why do we want to use virtual functions and abstract classes?

Abstract classes and virtual functions are useful for putting restraints on how the rest of your team creates code.

Virtual functions for programmers to implement specific functions with a set of defined parameters. By defining a function as virtual, we are telling the compiler to ignore that fact that the function is not implemented in this class.


# Accessing members through pointers

Recall that we can create a pointer to a type using the qualifier ( `*` ).

```c++
SomeClass * object_ptr;
object_ptr = new SomeClass("a string argument");
```

In order to access member variables and functions of this class we must first dereference the class then access the member. C++, for some reason, doesn't do that for us.

```c++
(*object_ptr).memberVariable = "Something else";
(*object_ptr).setNumber(1);
```

This is a bit clunky so we are given a member access operator for pointers ( `->` ). The following two lines of code are equivalent:

```c++
(*object_ptr).memberVariable = "Something else";

object_ptr -> memberVariable = "Something else"; 
```

