# Chapter 11 (Inheritance and Composition)

***Inheritance*** means that the class inherits properties from another class. In the book this is called an *"is-a"* relationship.

***Composition*** or ***aggregation*** means that the class has members of some class. In the book, this called a *"has-a"* relationship.

## Inheritance 

Inheritance allows us to make new classes from current classes.

> These new classes are called ***derived classes***. The classes they are derived from are called ***base classes***.

The syntax for a derived class is:

```c++
class DerivedClassName: memberAccessSpecifier BaseClassName
{
    // Members
};
```

... where **memberAccessSpecifier** can be `public`, `protected`, or `private`.

> By default (when no member access specifier is given), the inheritance is assumed to be `private`.

### `public` and `private` inheritance

The inheritance specifies what the public members of the base class will become after inheritance.

## Inheritance and private member variables

If we overload a member function of a base class, we may need to run the member function of base class in the overloaded function definition. To do this, we use the name of the base class followed by the scope resolution operator ( `::` ).

```c++
void DerivedClass::print() const
{
    BaseClass::print();
}
```

## `protected` members

The access of a `protected` member is between `public` and `private`. When we create a derived class, we have direct access to the public members of the base class, but we need *accessors* and *mutators* to modify the private members.

> We used private members to prevent direct access outside of a class.

With `protected` members, we can *prevent direct access from outside of a class* **AND** *provide direct access to the member to derived classes*.