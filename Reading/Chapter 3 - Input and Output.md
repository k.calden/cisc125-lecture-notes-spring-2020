# Chapter 3 ( Input/Output )
## Streams 
`cin` is defined as an `istream` or ***input stream type***

`cout` is defined as an `ostream` or ***output stream type***
### `get(var)` function
The ( `>>` ) operator skips whitespace. In order to grab the entire stream we must use `.get()` on the input stream.

```c++
cin.get(var)
```

### `put(var)` function
Using `get(var)` allows us to use the `putback(var)` function to put the last thing removed back into the stream from the argument given


### `peek()` function
We can use `peek()` to get the value from a stream without removing it from the stream.


### `clear()` function
`clear()` is used to clear the stream

## Formatting
The output can be formatted by using ***manipulators***. These are sent to an output stream to format it.

To use the manipulators we must include `iomanip`.

> `setprecision(n)`  sets the maximum number of decimal places.
> 
> `fixed` can be sent to output in a fixed decimal format and can be unset using `cout.unsetf(ios::fixed)`
> 
> `scientific` formats floats in scientific notation
> 
> `setw(n)` formats the outputs on a single line as columns of width ***n***


## Streaming files
We can interact with files using the `fstream` library. This gives us two data types: 

`ifstream` for ***input files***

`ofstream` for ***outputting files***

We can use the `>>`  and `<<` operators to interact with the file streams just like we would with `cout` and `cin`.

Including `fstream` automatically includes `iostream` so we don’t NEED to `#include` it, but we should still use the `std` namespace.
