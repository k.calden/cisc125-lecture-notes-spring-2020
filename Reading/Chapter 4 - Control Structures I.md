# Chapter 4 ( Control Structures I )

Already understand these concepts: 
- `switch` statements
- ***comparison operators***
- `if`, `then`, `else` statments
## Comparing strings
When using `>`, `<`, or `==` on strings. The program sequentially checks the ASCII value of each character and evaluates to false if the comparison is not met on a character.

## Program termination ( assertion )
We can use the `assert(expression)` function to terminate the program when that expression evaluates to true.