# Chapter 2 (Elements of C++)

## Console Output

We can right things to the console of a C++ using the iostream library. 

```c++
#include <iostream>
```

> This library alows us to use ```cout``` (print output) and ```endl``` (move to the next line)

`cout` and `endl` are part of the `std` namespace, therefore, we must write them as `std::cout` if we don't put `using namespace std;` at the top of our program.

> The ( ```::``` ) symbol tells the compiler that `cout` is a member of the `std` namespace.

`cout` is a stream so we are just sending data to it to be put into the console using the ***stream insertion operator*** ( `<<` )

```c++
#include <iostream>
using namespace std;

cout << "Hello world!" << endl;
```

or 

```c++
#include <iostream>

std::cout << "Hello world!" << std::endl;
```

## Some Basic Syntax
### Functions

We specify functions using the format: 
```c++
returnType functionName(type args, type stuff){
    //code; 
    returnType returns = stuff;
    return returns; 
}
```

	
>Every C++ program must have a `main()` function. This function runs all of the code.

### Casting ( `static_cast<type>(expression)` )

We can cast an expression to a certain type using the `static_cast` keyword.
```c++
static_cast<type>(expression)
```
### Named constants ( `const` )
We can use `const` to specify some data that stays the same throughout the program.

### Reading data in ( `cin` )
We can use `cin` to read data from the keyboard.

> `cin` is also a stream except we have to use ( `>>` ) to extract data from it. This is called the ***stream extraction operator***.

```c++
cin >> A >> B
```

is equivalent to 

```c++
cin >> A
cin >> B
```