# Chapter 1 (Programming languages)

First chapter talks about the relation ship between the CPU, memory, and peripherals.

## C++ Programs 

Statements that begin with **#** are called **preprocessor directives**.

The **compiler** checks the program for syntax errors and translates it into the equivalent machine language. It's then converted in to an **object program** which is the machine language version of the high level program.

A **library** is a set of prewritten code. A **linker** combines all of the code into a library.

A **loader** is a set of prewritten code. A **linker** combines all of the code into a library.

A loader loads the program into the memory where it can be executed.