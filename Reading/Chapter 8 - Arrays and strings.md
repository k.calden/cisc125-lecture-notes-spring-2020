# Chapter 8 (Arrays and strings)

Already know about:
- Indexing arrays
- Multidimensional arrays
- Sequential search
- Selection sort

## Passing arrays into functions

In C++, functions can't return arrays so they must be passed into a function by reference and modified.

```c++
void arrayFunc(int list[]) {
    //
}
```

`list` is passed into the function by reference. The function can then modify whatever array is passed in.

If we don't want a function to modify an array we can use the `const` keyword before `int`.

## Note on character arrays

We can create character arrays using: 

```c++
char chrArray[] = "Something";
```

> We must use character arrays to specify a filename.