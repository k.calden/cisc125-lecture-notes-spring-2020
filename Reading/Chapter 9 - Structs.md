# Chapter 9 (Records / structs)

A `struct`, like a `namespace`, is a collection of components; however, a `struct` doesn't include functions.

```c++
struct someNewType
{
    int thingA;
    double thingB;
};
```

A `struct` is a definition of a new datatype. When a variable is defined as the new type, no memory is allocated.

```c++
someNewType structVariable;
```

We can access members of a `struct` using the ***member access operator*** ( `.` ).

> A struct can be a member of another struct.

```c++
structVariable.thingA = 1;
```

We can copy the contents of one struct into another using the assignment operator ( `=` ). 

> We can't use comparison operators on them though.
