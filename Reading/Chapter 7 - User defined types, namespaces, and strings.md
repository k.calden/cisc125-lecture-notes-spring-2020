# Chapter 7 (User defined types, namespaces, and strings)
## Enumeration types
***Enumeration types*** are simple user defined data types. To define an enumeration type we use the `enum` keyword and name members of that type.

```c++
enum typeName {THINGA, THINGB, THINGC};
```

Then we can use the type like normal: 
```c++
typeName varA = THINGA;
```

## `typedef`
We can use `typedef` to make aliases for already available types. For example, we can create a boolean type that corresponds to integer values by using a typedef.

```c++
typedef int Boolean;
const Boolean TRUE = 1;
const Boolean FALSE = 0;
```

Defining a variable as our boolean type will force us to make it either `TRUE` or `FALSE`.

## Namespaces

***Namespaces*** are collections of constants, variable, declarations, function and other namespaces. They are used to separate global identifiers.

We can specify that we are using a namespace or member of a names using: 

```c++
using namespace namespace_name;
```

or

```c++
using namespace_name::identifier;
```

Either way, we would be able to access `identifier` using `identifier` instead of `namespace_name::identifier`.

