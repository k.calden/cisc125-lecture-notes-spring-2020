# Chapter 10 - Classes and Data Abstraction

## Classes

***Classes***, like ***structs***, are another collection of data. Classes group data and functions. 

```c++
class SomeClass
{
    // Class members
};
```

Members of a class can be `private`, `public`, and `protected`.

> By default, all members are `private`, which means it can't be accessed outside of the class.

> `public` members are accessible from outside the class

We specify the access by putting the access type as a header with a colon above the members:

```c++
class SomeClass
{
    public:
        int A;
        double B;
        int getC();
        void setC(int);
        int equalC(int) const;
    private:
        int C;
        double D
};
```

> `const` at the end of the member functions specifies that we can't modify the members of the class.

Classes are types and can be declared as such.

To access public members of the class, I can use the ***member access operator*** ( `.` ).

## Implementing member functions

In the above code, I only put the function prototype into the class, but I could have just put the implementation as well.

This would make the class definition really long. So I will just write the definition separate from the class using the ***scope resolution operator*** ( `::` ).

> I can just write these implementations anywhere else as long as the function prototypes are given before the class is used.

```c++
int SomeClass::getC() {
    return C;
}
```

> I can just refer to the members of the class directly in the implementation.

## Accessors and mutators

***Accessor functions*** access member variables of functions.

> Should put `const` at the end of these functions as a safeguard because they are only accessing the members. (these are called ***constant functions***)

***Mutator functions*** modify the values of member variables

## Constructors

Constructors are used to run code when a class is instantiated. They are defined as functions with no return type and have the same name as the class itself.

```c++
class SomeClass
{
    int A;
    int B;
    int C;

    SomeClass(int, int, int)
};

SomeClass::SomeClass(int a, int b, int c)
{
    A = a;
    B = b;
    C = c;
}
```

### Invoking the constructor

When we instantiate a class like we would any other type, the ***default constructor*** is run.

To invoke another construct we put the arguments after the name of the instance.

```c++
SomeClass someInstance(1, 2, 3);
```

> This runs the constructor that we implemented above.

## Destructors

***Destructors*** are also functions that don't have types. Destructors can be declared by prefixing the name of the class with a tilde ( `~` ).

```c++
~SomeClass();
```

## Abstract data types

***Abstract data types*** just separate the properties from teh implementation. We did this above when we only declared the members of the classes and only wrote the function prototypes.

## Static members

Remember that the `static` keyword was used to force local variable to persist between function calls.

Members of classes can be declared as static using the `static` keyword as well.

```c++
class SomeClass
{
    public:
        static int numberOfInstances;
    private:
        int A;
        int B;
    
    SomeClass(int, int);
};

SomeClass::SomeClass() {
    A = 0;
    B = 0;
    numberOfInstances++;
}

SomeClass::SomeClass(int a, int b) {
    A = a;
    B = b;
    numberOfInstances++;
}
```

> Static members have one memory location, therefore, modifying the value in one instance changes it for all instances.