# Chapter 6 ( User-defined functions )
Already understand these concepts: 
- Defining functions
- `return` values
- `void` functions
- Object ***scope***
- ***function overloading***

## Function prototypes

It looks nicer when programmers put function definitions after the main function. This can produce compiler errors because the functions in a program are **compiled in the order in which they are seen**. 

To keep this style we want to define ***function prototypes*** before the main function that tells the compiler that the function exists and its parameters and returns.

To write a function prototype we just write out the ***function signature*** before the main function.

```c++
type functionName(typeA A, typeB B);
```

Since the prototype isn’t a definition we don’t need to put the variable names in (`A` and `B`).

```c++
type functionName(typeA, typeB);
```


## Value and reference parameters
When we define a function, we can specify whether a parameter is a copy of the value of whatever is passed in ( ***value parameter*** ) or a memory address ( ***reference parameter*** ).

### Reference parameters

The parameter can be specified as a reference parameter by putting `&` **after the type**.

```c++
void variableAlteringFunction(int& A) {
	A = 2;
}
```

The function above sets the value at the memory address to 2.

When a variable is given as the argument to that function, the function is then able to modify it.

**Streams should always be passed by reference to a function**.

## Static and automatic variables
***Static variables*** are only initialized once and their memory is allocated once. Variables declared outside of functions are static by default.

Variables declared inside functions are ***automatic variables*** by default. These have their memory allocated in the function and unallocated when the function ends. We can use the `static` keyword before the variable declaration to make a variable inside a function static.

When a variable is declared as static in a function it is only allocated and initialized on the first call of the function. Running the function again skips over the variable definition.

## Default parameters
We can specify default parameters in a function for cases where the argument is omitted. The caveat is that these default parameters must all be on the right side.

> (GOOD) `void func(int A, double B = 5.2, double C = 3.4)`
> 
> (BAD) `void func(int A = 5, double B, double C = 3)`
> 
> (BAD) `void func(int A = 1, double B)`
